// 1. Як можна оголосити змінну у Javascript? 
// let, const, var
// 2. У чому різниця між функцією prompt та функцією confirm?
// promt дає можливість ввести дані, а confirm дає можливість лише відповісти OK/CANSEL
// 3. Що таке неявне перетворення типів? Наведіть один приклад.
// неявне перетворення - перетворення типів під час виконнання умови.
// Наприклад коли додаєш число до строки то число перетворюється на строку.

let admin;
const name = 'Vika';
admin = name;
console.log(admin);

const days = 9;
const HOURS_IN_DAY = 24;
const MINUTES_IN_DAY = 60;
const SECONDS_IN_MINUTE = 60;
const seconds = days * HOURS_IN_DAY * MINUTES_IN_DAY * SECONDS_IN_MINUTE;
console.log(seconds);

const answer = prompt("It's you?");
console.log(answer);
